#!/usr/bin/python
import argparse
import glob
import os
import subprocess
import sys
import time
from pathlib import Path
import shutil
import mimetypes

parser = argparse.ArgumentParser(description='Check if AVC(H264) is bad compressed and encode it')
parser.add_argument('target', help='file or directory for compress')
parser.add_argument("--dry", help="only show what have to do", action="store_true")

try:
    options = parser.parse_args()
except:
    parser.print_help()
    sys.exit(0)

dry_run = options.dry
# dry_run = True
if dry_run:
    print("It is dry run")

print("Get target: ", options.target)
if os.path.isdir(options.target):
    files = glob.glob(options.target + '/**/*', recursive=True)
elif os.path.isfile(options.target):
    files = [options.target]
else:
    print("target is a special file (socket, FIFO, device file)")
    sys.exit(0)


media_files = []
for file in files:
    mimetype = mimetypes.guess_type(file)[0]
    if mimetype.startswith('video/'):
        media_files.append(file)


if len(media_files) == 0:
    print("There is no video files")
    sys.exit(0)


print("")
files_recompress = []
for file in media_files:
    print("check ", file)
    ffprobe = subprocess.Popen(
        "ffprobe -v error -select_streams v:0 -show_entries stream=profile -of default=noprint_wrappers=1:nokey=1 " + file,
        shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    ffprobe_stdout, ffprobe_stderr = ffprobe.communicate()

    if ffprobe.returncode != 0:
        print("\tfail to get avc profile")
        continue

    avc_profile = ffprobe_stdout.splitlines()[0]

    need_recompress = False
    if "Baseline" in avc_profile:
        need_recompress = True
    elif "Main" in avc_profile:
        need_recompress = True
    print("\trecompress_need: ", need_recompress)

    if need_recompress:
        files_recompress.append(file)


print("")
error_files = []
for file in files_recompress:
    print("processing ", file)
    file_size = round(os.stat(file).st_size / (1024*1024))
    print("\tsize ", file_size, "Mb")
    file_extension = ''.join(Path(file).suffixes)
    tmp_output_file = str(Path(file).parent.absolute()) + "/tmp_output" + file_extension
    cmd = "ffmpeg -i " + file + " -c:v libx264 -preset slow -c:a copy " + tmp_output_file

    if dry_run:
        print("\t", cmd)
        continue

    ffmpeg = subprocess.Popen(cmd,
                              shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    ffmpeg_stdout, ffmpeg_stderr = ffmpeg.communicate()

    if ffmpeg.returncode != 0:
        print("\tCan't encode")
        os.remove(tmp_output_file)
        error_files.append(file)
        continue

    encode_size = round(os.stat(tmp_output_file).st_size / (1024 * 1024))
    print("\tencode done. size ", encode_size, "Mb")
    print("\tReplace original after 3 seconds...")
    time.sleep(3)

    print("\tReplacing original file")
    shutil.move(tmp_output_file, file)


print("\nAll done")
if error_files:
    print("Errors. Not recompress files:")
    for file in error_files:
        print(file)